/**
 * Created by roman on 23.10.14.
 */
"use strict";

var dateFormat = require('dateformat');

module.exports = function (sequelize, DataTypes) {
    var time = sequelize.define("time", {
            id: {type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
            //worker_id: {type: DataTypes.INTEGER, allowNull: false},
            date: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
            start: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: '0001-01-01 00:00:00'
            },
            end: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: '0001-01-01 00:59:59'
            }
        },
        {
            updatedAt: false,
            createdAt: false,
            classMethods: {
                associate: function (models) {
                    time.belongsTo(models.worker, {foreignKey: 'worker_id', constraints: false}); //������ � ����� � �� ����� foreignKey ��� ����������� �� �� 1 �����, � �������� ������
                }
            },
            instanceMethods: {
                getStringDate: function () {
                    return dateFormat(this.getDataValue('date'), "yyyy-mm-dd");
                },
                getStringStartTime: function () {
                    return dateFormat(this.getDataValue('start'), "HH:MM");
                },
                getStringEndTime: function () {
                    return dateFormat(this.getDataValue('end'), "HH:MM");
                }
            }
            /*indexes: [{
             name: 'worker_id',
             fields: ['worker_id']
             }]*/
        });
    return time;
}