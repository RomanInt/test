var express = require('express');
var router = express.Router();
var models = require("../models");
var util = require("../libs/util");

/* GET workers listing */
router.get('/', function (req, res, next) {
    var page = 1,
        limit = 100;
    var countTimes = 0;
    if (!isNaN(req.query.page)) {
        page = parseInt(req.query.page, 10);
    }
    var where = {};
    models.worker.count({where: where}).then(function (count) {
        countTimes = count;
        return models.worker.findAll({
            where: where,
            limit: util.getLimit(page, limit)
        })
    }).then(function (workers) {
        var data = {
            title: 'Workers',
            workers: workers,
            query: req.query,
            page: page,
            pageCount: Math.floor(1 + countTimes / limit)
        };
        if (req.query.json) {
            res.send(data);
        } else {
            res.render('workers', data);
        }
    }).catch(function (err) {
        next(err);
    });
});

/* GET worker by id */
router.get('/:id', function (req, res, next) {
    models.worker.findById(req.params.id).then(function (worker) {
        var data = {
            title: 'Worker ' + (worker ? worker.first_name : ""),
            workers: worker ? [worker] : [],
            query: req.query,
            page: 1,
            pageCount: 1
        };
        if (req.query.json) {
            res.send(data);
        } else {
            res.render('workers', data);
        }
    }).catch(function (err) {
        next(err);
    });
})
router.post('/:id/delete', function (req, res, next) {
    models.worker.destroy({
        where: {
            id: req.params.id
        }
    }).then(function () {
        if (req.query.json) { //restful to angular ore ajax query
            res.send({success: true});
        } else {
            res.redirect('/workers');
        }
    }).catch(function (err) {
        next(err);
    });
})
router.post('/find', function (req, res, next) {
    models.worker.findOne({
        where: {
            last_name: {
                $like: "%" + req.body.name + "%"
            }
        }
    }).then(function (worker) {
        if (req.query.json) {
            res.send(worker);
        } else {
            res.redirect('/workers/' + (worker ? worker.id : null));
        }
    }).catch(function (err) {
        next(err);
    });
})

router.post('/:id/save', function (req, res, next) {
    models.worker.findById(req.params.id).then(function (object) {
        if (object) {
            for (var p in req.body) {
                if (object[p] != null && typeof object[p].getMonth === 'function') {
                    object[p] = new Date(req.body[p]);  // date �������� �������
                } else {
                    object[p] = req.body[p];
                }
            }
            return object.save();
        } else {
            return models.worker.create(req.body);
        }
    }).then(function (worker) {
        if (req.query.json) {
            res.send(worker);
        } else {
            res.redirect('/workers');
        }
    }).catch(function (err) {
        next(err);
    });
})


module.exports = router;
